class Node {
    constructor(data, left = null, right = null) {
        this.data = data
        this.left = left
        this.right = right
    }
}

class BST {
    constructor() {
        this.root = null
    }

    add(data) {
        const node = this.root
        if (node === null) {
            this.root = new Node(data)
            return
        }
        else {
            const search = node => {
                // Smaller numbers on the left, larger on the right.
                // Check for smaller/left:
                if (data < node.data) {
                    if (node.left === null) {
                        node.left = new Node(data)
                        return
                    }
                    else {
                        return search(node.left)
                    }
                }

                // Check for larger/right:
                else if (data > node.data) {
                    if (node.right === null) {
                        node.right = new Node(data)
                    }
                    else {
                        return search(node.right)
                    }
                }
                
                // Data is the same as the node value
                else {
                    return null
                }
            }
            return search(node)
        }
    }

    min() {
        let current = this.root
        while (current.left !== null) {
            current = current.left
        }
        return current.data
    }

    // max
    max() {
        let current = this.root
        while (current.right !== null) {
            current = current.right
        }
        return current.data
    }

    // Traverse the tree to find the target
    find(target) {
        let current = this.root
        while (current) {
            if (target === current.data) {
                return true
            }
            else if (target < current.data) {
                current = current.left
            }
            else {
                current = current.right
            }
        }
        return false
    }

    remove(data) {
        const removeNode = (node, data) => {
            if (node == null) {
                return null
            }
            // Find the target to delete
            if (node.data == data) {
                // Childess node
                if (node.left === null && node.right === null) {
                    return null
                }
                
                // 1 child: return the node that exists
                if (node.left === null || node.right === null) {
                    return node.left === null ? node.right : node.left
                }

                // 2 children
                let temp = node.right
                while (temp.left !== null) {
                    temp = temp.left
                }
                node.data = temp.data
                node.right = removeNode(node.right, temp.data)
            }
            else if (node.data < data) {
                node.right = removeNode(node.right, data)
            }
            else {
                node.left = removeNode(node.left, data)
            }
            return node
        }
        this.root = removeNode(this.root, data)
    }
}

let foo = new BST()

let nums = [12, 45, 322, 923, 132, 12, 22, 44, 29, 91, 24, 5, 2, 6, 8, 10]
nums.map(x => foo.add(x))

console.log(JSON.stringify(foo, null, 4))