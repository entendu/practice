// hash function
let hash = (k, len) => {
    let out = 0
    for (var i = 0; i < k.length; i++) {
        out += k.charCodeAt(i)
    }
    return out % len
}

// Hashtable class
let HashTable = function () {
    let buckets = 5
    let storage = []

    this.print = () => {
        console.log(storage)
    }

    this.state = () => storage

    this.add = (key, val) => {
        let hashVal = hash(key, buckets)
        if (storage[hashVal] === undefined) {
            storage[hashVal] = [[ key, val ]]
        }
        else {
            var inserted = false
            for (var i = 0; i < storage[hashVal].length; i++) {
                if (storage[hashVal][i][0] === key) {
                    storage[hashVal][i][1] = val
                    inserted = true
                }
            }
            if (!inserted) {
                storage[hashVal].push([ key, val ])
            }
        }
    }

    this.delete = key => {
        let hashVal = hash(key, buckets)
        if (storage[hashVal] === undefined) {
            return
        }
        else {
            for (var i = 0; i < storage[hashVal].length; i++) {
                if (storage[hashVal][i][0] === key) {
                    delete storage[hashVal][i]
                    return
                }
            }
            delete storage[hashVal]
            return   
        }
    }

    this.lookup = key => {
        let hashVal = hash(key, buckets)
        if (storage[hashVal] !== undefined) {
            for (var i = 0; i < storage[hashVal].length; i++) {
                if (storage[hashVal][i] !== undefined && storage[hashVal][i][0] === key) {
                    return storage[hashVal][i][1]
                }
            }
        }
        return undefined
    }
}

module.exports = { hash: hash, HashTable: HashTable }
