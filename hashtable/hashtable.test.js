const ht = require('./hashtable')

test('Can create HT', () => {
    let table = new ht.HashTable()
    expect(table.constructor.name).toBe("HashTable")    
})

test('Can add items', () => {
    let table = new ht.HashTable()
    table.add("hey", "you")
    expect(table.state()).toEqual(
        expect.arrayContaining([
            expect.arrayContaining([
                expect.arrayContaining([
                    "hey", "you"
                ])
            ])
        ])
    )
})

test('Can remove items', () => {
    let table = new ht.HashTable()
    table.add("hey", "you")
    table.add("foo", "bar")
    table.add("baz", "buzz")
    table.delete("hey", "you")
    expect(table.state()).not.toEqual(
        expect.arrayContaining([
            expect.arrayContaining([
                expect.arrayContaining([
                    "hey", "you"
                ])
            ])
        ])
    )
})

test('Add, remove, lookup', () => {
    let table = new ht.HashTable()
    table.add("hello", "there")
    table.add("general", "kenobi")
    table.add("oellh", "where")
    table.delete("hello")
    
    expect(table.lookup("oellh")).toBe("where")
})
